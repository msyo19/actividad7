package com.example.msyo19.practicaandroid;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText telefonl;
    EditText urll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        telefonl = (EditText) findViewById(R.id.editText);
        urll = (EditText) findViewById(R.id.editText2);
    }

    public void telefono(View view ){
        String numero= telefonl.getText().toString();
        if(TextUtils.isDigitsOnly(numero)&& numero.length() == 10 ){
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+numero));
            startActivity(intent);
        }
        else{
            telefonl.setError("debe ser un telefono valido");
        }
    }
    public void url(View view ){
        String url= urll.getText().toString();
        if(URLUtil.isValidUrl(url)){
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }else{
            urll.setError("debe ser un telefono valido");
        }
    }
    public void map(View view ){
        Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
        Intent intentm = new Intent(Intent.ACTION_VIEW,gmmIntentUri);
        intentm.setPackage("com.google.android.apps.maps");
        startActivity(intentm);
    }
}
